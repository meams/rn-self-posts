import React, { useState } from 'react'
import { useFonts, Roboto_400Regular, Roboto_700Bold } from '@expo-google-fonts/roboto';
import { StyleSheet, Text, View } from 'react-native'
import { AppLoading } from 'expo'
import AppNavigation from './src/navigation/AppNavigation'

export default function App() {
  let [fontsLoaded] = useFonts({
    Roboto_400Regular, Roboto_700Bold
  });

  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <AppNavigation />
  )
}
