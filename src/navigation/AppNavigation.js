import React from 'react'
import { Platform, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MainScreen } from '../screens/MainScreen'
import { PostScreen } from '../screens/PostScreen'
import { BookedScreen } from '../screens/BookedScreen'
import { AboutScreen } from '../screens/AboutScreen'
import { CreateScreen } from '../screens/CreateScreen'
import { THEME } from '../theme'
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
import { AppHeaderIcon } from '../components/AppHeaderIcon'
import { Ionicons } from '@expo/vector-icons'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'

const Stack = createStackNavigator()
const Tab = createMaterialBottomTabNavigator();
const Drawer = createDrawerNavigator();

const stackNavigationOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? THEME.MAIN_COLOR : '#fff'
  },
  headerTintColor: Platform.OS == 'android' ? '#fff' : THEME.MAIN_COLOR
}

const MainScreenTab = () => (
  <Stack.Navigator
    initialRouteName="Main"
    screenOptions={stackNavigationOptions}
  >
    <Stack.Screen
      name="Main"
      component={MainScreen}
      options={({ navigation }) => ({
        title: 'My blog',
        headerRight: () => (
          <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title='Take photo' iconName='ios-camera' onPress={() => navigation.navigate('Create')} />
          </HeaderButtons>
        ),
        headerLeft: (props) => (
          <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title='Toggle drawer' iconName='ios-menu' onPress={navigation.toggleDrawer} />
          </HeaderButtons>
        )
      })}
    />
    <Stack.Screen
      name="Post"
      component={PostScreen}
      options={({ route, navigation }) => ({
        title: `Post from ${new Date(route.params.date).toLocaleDateString()}`,
        headerRight: () => {
          const iconName = route.params.booked ? 'ios-star' : 'ios-star-outline'
          return (
            <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
              <Item title='Take photo' iconName={iconName} onPress={() => console.log('photo')} />
            </HeaderButtons>
          )
        },
      })}
    />
  </Stack.Navigator>
)

const BookedScreenTab = () => (
  <Stack.Navigator
    initialRouteName="Booked"
    screenOptions={stackNavigationOptions}
  >
    <Stack.Screen
      name="Booked"
      component={BookedScreen}
      options={({ navigation }) => ({
        title: 'Favorites',
        headerLeft: () => (
          <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title='Toggle drawer' iconName='ios-menu' onPress={navigation.toggleDrawer} />
          </HeaderButtons>
        )
      })}

    />
    <Stack.Screen
      name="Post"
      component={PostScreen}
      options={({ route, navigation }) => ({
        title: `Post from ${new Date(route.params.date).toLocaleDateString()}`,
        headerRight: () => {
          const iconName = route.params.booked ? 'ios-star' : 'ios-star-outline'
          return (
            <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
              <Item title='Take photo' iconName={iconName} onPress={() => console.log('photo')} />
            </HeaderButtons>
          )
        },
      })}
    />
  </Stack.Navigator>
)

const MainScreenTabWrap = () => (
  <Tab.Navigator
    tabBarOptions={{ activeTintColor: THEME.MAIN_COLOR, /*inactiveTintColor: 'gray'*/ }}
    shifting={true}
    barStyle={{ backgroundColor: THEME.MAIN_COLOR }}>
    <Tab.Screen
      name='Post'
      component={MainScreenTab}
      options={{
        tabBarLabel: 'All',
        tabBarIcon: ({ color, size }) => {
          return <Ionicons name='ios-albums' size={24} color={color} />;
        }
      }}
    />
    <Tab.Screen
      name='Booked'
      component={BookedScreenTab}
      options={{
        tabBarLabel: 'Favorites',
        tabBarIcon: ({ color, size }) => {
          return <Ionicons name='ios-star' size={24} color={color} />;
        }
      }}
    />
  </Tab.Navigator>
)

const AboutScreenStack = () => (
  <Stack.Navigator
    initialRouteName="About"
    screenOptions={stackNavigationOptions}>
    <Stack.Screen
      name="About"
      component={AboutScreen}
      options={({ navigation }) => ({
        title: 'About',
        headerLeft: (props) => (
          <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title='Toggle drawer' iconName='ios-menu' onPress={navigation.toggleDrawer} />
          </HeaderButtons>
        )
      })}
    />
  </Stack.Navigator>
)

const CreateScreenStack = () => (
  <Stack.Navigator
    initialRouteName="About"
    screenOptions={stackNavigationOptions}>
    <Stack.Screen
      name="Create"
      component={CreateScreen}
      options={({ navigation }) => ({
        title: 'Create',
        headerLeft: (props) => (
          <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item title='Toggle drawer' iconName='ios-menu' onPress={navigation.toggleDrawer} />
          </HeaderButtons>
        )
      })}
    />
  </Stack.Navigator>
)

const AppNavigation = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        drawerContentOptions={{
          activeTintColor: THEME.MAIN_COLOR,
          labelStyle: {
            fontFamily: 'Roboto_700Bold'
          }
        }}>
        <Drawer.Screen
          name="Posts"
          component={MainScreenTabWrap}
          options={{
            drawerLabel: 'Main',
            // drawerIcon: () => <Ionicons name='ios-star' />
          }}
        />
        <Drawer.Screen
          name="About"
          component={AboutScreenStack}
          options={{ drawerLabel: 'About application' }}
        />
        <Drawer.Screen
          name="Create"
          component={CreateScreenStack}
          options={{ drawerLabel: 'New post' }}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigation